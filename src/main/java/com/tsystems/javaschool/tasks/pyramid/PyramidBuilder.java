package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    private int[][] res;
    private int rows;
    private int columns;
    private List<Integer> inp;

    private int getColumnsAmount(int inpSize) {

        /*at every N-step we have N nonzero elements
        get sum of them as sum of arithmetic progression
        this sum should be equal to number of input elements*/

        int last_sum = 0;
        int sum = 0;
        int i = 1;

        while (sum < inpSize) {
            last_sum = sum;
            sum = (i + i * i) / 2;
            i++;
        }

        /*(last_sum - sum) gives us amount of elements at last step of pyramid/
        double it because we need to store zeros*/

        if (sum == inpSize) {
            rows = i - 1;
            return (sum - last_sum) * 2 - 1;
        }

        if (sum > inpSize)
        {
            throw new CannotBuildPyramidException("can't build pyramid from this amount of elements");
        }
        return 0;
    }

    private void fillRes(int cur_row, int last) {
        //if we've got all results
        if (cur_row == 0) {
            return;
        }

        //get necessary amount of element for current step of pyramid
        List<Integer> add = inp.subList(last - cur_row, last);

        int k = 0;
        int beg = rows - cur_row;
        int flag = beg % 2;

        //add elements at proper places
        for (int i = beg; i < columns; i++) {
            if ((i % 2) == flag) {
                res[cur_row - 1][i] = add.get(k);
                k++;
            }
            if (k == cur_row) break;
        }

        cur_row--;
        last = last - cur_row - 1;
        fillRes(cur_row, last);
    }

    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.size() > (Integer.MAX_VALUE - 2)){
            throw new CannotBuildPyramidException("Out of memory: too long input");
        }
        else if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException("contains NULL element");
        } else {
            Collections.sort(inputNumbers);
        }

        this.inp = inputNumbers;
        this.columns = getColumnsAmount(inputNumbers.size());

        res = new int[rows][columns];
        fillRes(rows, inputNumbers.size());

        return res;
    }
}
