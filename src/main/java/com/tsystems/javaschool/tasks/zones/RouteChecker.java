package com.tsystems.javaschool.tasks.zones;

import java.util.Collections;
import java.util.List;

public class RouteChecker {

    /*get maximum id of zonestates */
    private int getMax(List<Zone> zoneState){
        int max = 0;
        for (Zone state: zoneState){
            int id = state.getId();
            if (state.getId() > max) {
                max = id;
            }
        }
        return max;
    }

    /*in case of fail in building the route check if
    last of requested zones has no path to the first one*/
    private boolean checkCycle(int cur_id, List<Integer> requested, int[][] adj) {
        List<Integer> tail = requested.subList(cur_id, requested.size());
        for (Integer id : tail){
            if (adj[id][requested.get(0)] == 1) return true;
        }
        return false;
    }

    /*check if zonestate has zone with id*/
    private  boolean hasZone(List<Zone> zoneState, int id){
        for (Zone state: zoneState){
            int st = state.getId();
            if (st == id) { return true;}
        }
        return false;
    }

    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){

        Collections.sort(requestedZoneIds);

        //check if zonestates include all our requested zones
        for(int zoneId : requestedZoneIds){
            if (!hasZone(zoneState, zoneId))
                return false;
        }

        /*fill adjacency graph*/
        int max = getMax(zoneState);
        int[][] adj = new int [max + 1] [max + 1];
        for (Zone state: zoneState){
            List<Integer> nbs  = state.getNeighbours();
            for (Integer nb: nbs ){
                adj[state.getId()][nb] = 1;
                adj[nb][state.getId()] = 1;
            }
        }

        /*if there is no way between two neighbour vertices then
        there is no route. also we should be sure that there is no cycle,
        which include our last and first points (we've already checked
        a route from the begining to the end, and we need to check if
        there is no way like 1-2-5 and 5-1-2 in case 1-2 is ok and
        2-5 is a problem))
        */
        for (int i = 1 ; i < requestedZoneIds.size(); i++){
            int last = requestedZoneIds.get(i - 1);
            int cur = requestedZoneIds.get(i);
            if (adj[last][cur] != 1){
                if (!checkCycle(i, requestedZoneIds, adj))
                return false;
            }
        }

        return true;
    }

}
